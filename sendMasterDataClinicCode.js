const API = require("./config");
const https = require("https");
https.globalAgent.maxSockets = 5;

const fs = require("fs");
const logFile = require("./createLogFile");
//npm install
const dateFormat = require("dateformat");

//==================Since 2019-09-22======================//
// Created By: Famz
// Remark: หาต้องการสลับไปใช้ระบบ Test ให้เปลี่ยน https เป็น http ใน func. sendNotification และเปลี่ยน Host เป็นเลข IP: 203.154.116.83
//=======================================================//

exports.send = () => {
  getMasterDataClinicCode();
  function getMasterDataClinicCode() {
    var urldata = {
      host: API.HOSPITAL_SERVICE_HOST,
      path: API.HOSPITAL_SERVICE_PATH + "sendMasterDataClinicCode",
      // port: '443',
      method: "POST",
      rejectUnauthorized: false
    };

    const getReq = https.request(urldata, response => {
      var data = ""; //This will store the page we're downloading.
      response.on("data", chunk => {
        //Executed whenever a chunk is received.
        data += chunk; //Append each chunk to the data variable.
      });
      response.on("end", () => {
        var body = JSON.parse(data);
        const cliniccodeList = body.cliniccodelist;
        const token = body["token"];

        const jsonBody = JSON.stringify(cliniccodeList);

        var logData = {
          date: dateFormat(new Date(), "yyyy-mm-dd HH:MM:ss"),
          data: JSON.parse(jsonBody)
        };

        sendUpdateMasterDataClinicCode(token, jsonBody);
        logFile.createLogFile("A", "cliniccode", "cliniccode", logData);
        setTimeout(sendLineNotify, 20000);
      });
    });

    getReq.on("error", error => {
      console.error(error);
      API.LINE_NOTIFY.send({
        message:
          "⚠️ มีปัญหาบางอย่างเกิดขึ้น ไม่สามารถส่งอัพเดทข้อมูล clinic code ได้"
      });
    });

    getReq.end();
  }
};

function sendUpdateMasterDataClinicCode(token, jsonBody) {
  const sendNotification = {
    host: API.SCB_NOTIFICATION_HOST,
    path: API.SCB_NOTIFICATION_PATH + "receiveMasterDataClinicCode",
    method: "POST",
    headers: {
      Authorization: "Bearer " + token,
      "Content-Type": "application/json",
      lang: "TH",
      "Content-Length": Buffer.byteLength(jsonBody)
    }
  };
  const postReq = https.request(sendNotification, res => {
    var notiStatusCode;
    var data;
    res.on("data", chunk => {
      notiStatusCode = res.statusCode;
      data += chunk;
    });

    res.on("end", () => {
      // console.log(notiStatusCode);
      // console.log(data);
      if (notiStatusCode == "200") {
        var successBody = {
          statuscode: res.statusCode,
          date: dateFormat(new Date(), "yyyy-mm-dd HH:MM:ss"),
          result: Buffer.from(data).toString()
        };
        logFile.createLogFile(
          "A",
          "success",
          "cliniccode_success",
          successBody
        );
      } else {
        var errorBody = {
          statuscode: res.statusCode,
          date: dateFormat(new Date(), "yyyy-mm-dd HH:MM:ss"),
          result: Buffer.from(data).toString()
        };
        logFile.createLogFile("A", "error", "cliniccode_error", errorBody);
      }
    });
  });

  postReq.on("error", error => {
    console.error(error);
  });

  // //Sending...request body
  postReq.write(jsonBody);
  postReq.end();
}

function sendLineNotify() {
  let ts = Date.now();
  let date_ob = new Date(ts);
  let date = date_ob.getDate();
  let month = date_ob.getMonth() + 1;
  let year = date_ob.getFullYear();

  let monthStr = "";
  if (month <= 9) {
    monthStr = "0" + month.toString();
  } else {
    monthStr = month.toString();
  }

  let dateStr = "";
  if (date <= 9) {
    dateStr = "0" + date.toString();
  } else {
    dateStr = date.toString();
  }

  const errorFileName =
    "cliniccode_error" + "-" + year + "-" + monthStr + "-" + dateStr + ".json";
  const successFileName =
    "cliniccode_success" +
    "-" +
    year +
    "-" +
    monthStr +
    "-" +
    dateStr +
    ".json";
  try {
    if (
      !fs.existsSync("logs/error/" + errorFileName) &&
      fs.existsSync("logs/success/" + successFileName)
    ) {
      //file exists
      API.LINE_NOTIFY.send({
        message: "🏥 ส่งอัพเดทข้อมูล clinic code สำเร็จ"
      });
    } else if (
      fs.existsSync("logs/error/" + errorFileName) &&
      fs.existsSync("logs/success/" + successFileName)
    ) {
      API.LINE_NOTIFY.send({
        message:
          "🏥 ส่งอัพเดทข้อมูล clinic code สำเร็จ...แต่มีบาง clinic ไม่สามารถส่งได้ กรุณาตรวจสอบและทำการส่งใหม่อีกครั้ง"
      });
    } else if (!fs.existsSync("logs/success/" + successFileName)) {
      API.LINE_NOTIFY.send({
        message:
          "💥 ส่งอัพเดทข้อมูล clinic code ไม่สำเร็จ...กรุณาตรวจสอบการเรียกใช้งาน notification service และ การทำงานของ server"
      });
    }
  } catch (err) {
    console.error(err);
    API.LINE_NOTIFY.send({
      message:
        "💥 ส่งอัพเดทข้อมูล clinic code ไม่สำเร็จ...กรุณาตรวจสอบการเรียกใช้งาน notification service และ การทำงานของ server"
    });
  }
}
