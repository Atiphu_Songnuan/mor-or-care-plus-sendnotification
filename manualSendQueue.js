const API = require("./config");
const https = require("https");
https.globalAgent.maxSockets = 5;

const fs = require("fs");
const logFile = require("./createLogFile");
//npm install
const dateFormat = require("dateformat");

// exports.send = () => {
  getQueueData();
  function getQueueData() {
    var urldata = {
      host: API.HOSPITAL_SERVICE_HOST,
      path: API.HOSPITAL_SERVICE_PATH + "sendQueueNotification",
      // port: '443',
      method: "POST",
      rejectUnauthorized: false
    };

    const getReq = https.request(urldata, response => {
      var data = ""; //This will store the page we're downloading.
      response.on("data", chunk => {
        //Executed whenever a chunk is received.
        data += chunk; //Append each chunk to the data variable.
      });

      response.on("end", () => {
        var body = JSON.parse(data);
        const token = body["token"];
        var queueList = body["queuelist"];
        var queueNoti = [];

        if (queueList.length != 0) {
          queueList.forEach(queue => {
            var jsonBody = JSON.stringify(queue);
            queueNoti.push(JSON.parse(jsonBody));
          });

          //Get Current Date
          let ts = Date.now();
          let date_ob = new Date(ts);
          let date = date_ob.getDate();
          let month = date_ob.getMonth() + 1;
          let year = date_ob.getFullYear();

          let monthStr = "";
          if (month <= 9) {
            monthStr = "0" + month.toString();
          } else {
            monthStr = month.toString();
          }

          let dateStr = "";
          if (date <= 9) {
            dateStr = "0" + date.toString();
          } else {
            dateStr = date.toString();
          }

          const fileName =
            "queuechanged" +
            "-" +
            year +
            "-" +
            monthStr +
            "-" +
            dateStr +
            ".json";
          if (fs.existsSync("logs/" + "queuechanged/" + fileName)) {
            fs.readFile(
              "logs/" + "queuechanged/" + fileName,
              "utf8",
              (err, data) => {
                if (err) throw err;
                var queueChanged = JSON.parse(data);
                queueChanged.forEach(element1 => {
                  queueNoti.forEach(element2 => {
                    if (JSON.stringify(element2) == JSON.stringify(element1)) {
                      queueNoti.splice(queueNoti.indexOf(element2), 1);
                    }
                  });
                });

                if (queueNoti.length != 0) {
                  console.log("Queue changed");
                  console.log("==============================");
                  // *****Send QueueNotification*****
                  sendQueueNotification(token, queueNoti);
                  try {
                    fs.writeFile(
                      "logs/queuechanged/" + fileName,
                      JSON.stringify(queueNoti) + "\n",
                      function(err) {
                        if (err) throw err;
                      }
                    );
                  } catch (err) {
                    console.error(err);
                  }
                } else {
                  console.log("Queue not change");
                  console.log("==============================");
                }
              }
            );
          } else {
            //Create new log file
            //*****Send QueueNotification*****
            sendQueueNotification(token, queueNoti);
            logFile.createLogFile(
              "W",
              "queuechanged",
              "queuechanged",
              queueList
            );
          }
        } else {
          console.log("***Out of queue***");
          console.log("==============================");
        }
      });
    });

    getReq.on("error", error => {
      console.error(error);
    });
    getReq.end();
  }

  function sendQueueNotification(token, queueNoti) {
    var logdata = {
      date: dateFormat(new Date(), "yyyy-mm-dd HH:MM:ss"),
      data: []
    };

    // console.log(queueNoti);
    queueNoti.forEach(element => {
      var jsonBody = JSON.stringify(element);
      const sendNotification = {
        host: API.SCB_NOTIFICATION_HOST,
        path: API.SCB_NOTIFICATION_PATH + "receiveQueueNotification",
        method: "POST",
        headers: {
          Authorization: "Bearer " + token,
          "Content-Type": "application/json",
          lang: "TH",
          "Content-Length": Buffer.byteLength(jsonBody)
        }
      };

      logdata["data"].push(JSON.parse(jsonBody));
      const postReq = https.request(sendNotification);
      // response.setEncoding("utf8");
      // response.on("data", function(chunk) {
      //   //Executed whenever a chunk is received.
      //   console.log(chunk);
      //   // console.log(JSON.parse(chunk)["messageStatus"]);
      //   // console.log(JSON.parse(chunk)["messageDescription"]);
      //   // logdata[0]["response"].push(JSON.parse(chunk)["messageStatus"]);
      //   // logdata[0]["description"].push(JSON.parse(chunk)["messageDescription"]);
      // });

      postReq.on("error", error => {
        console.error(error);
      });
      // //Sending...request body
      postReq.write(jsonBody);
      postReq.end();
    });

    logFile.createLogFile(
      "A",
      "queuenotification",
      "queuenotification",
      logdata
    );
  }
// };
