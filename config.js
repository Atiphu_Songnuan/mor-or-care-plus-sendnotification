const LineAPI = require("line-api");

module.exports = {
  //=============== Hospital Service ===============//
  HOSPITAL_SERVICE_HOST: "his01.psu.ac.th",
  HOSPITAL_SERVICE_PATH: "/HosApp/dev/?/",
  //================================================//

  //=============== Notification Service (SCB) ===============//
  // SCB_NOTIFICATION_HOST: "mororcare.medicine.psu.ac.th",
  SCB_NOTIFICATION_HOST: "203.154.116.83",
  SCB_NOTIFICATION_PATH: "/ws/rest/",
  //===========================================================//

  LINE_NOTIFY: new LineAPI.Notify({
    token: "3GoeLZ3euxOWOjT2ojjRX7pPaDwPMjEZBO9RnxEHXVA"
  })
};
