const API = require("./config");
const https = require("https");
https.globalAgent.maxSockets = 5;

const fs = require("fs");
const logFile = require("./createLogFile");
//npm install
const dateFormat = require("dateformat");

exports.send = () => {
  getQueueData();
  function getQueueData() {
    var urldata = {
      host: API.HOSPITAL_SERVICE_HOST,
      path: API.HOSPITAL_SERVICE_PATH + "sendQueueNotification",
      // port: '443',
      method: "POST",
      rejectUnauthorized: false
    };

    const getReq = https.request(urldata, response => {
      var data = ""; //This will store the page we're downloading.
      response.on("data", chunk => {
        //Executed whenever a chunk is received.
        data += chunk; //Append each chunk to the data variable.
      });

      response.on("end", () => {
        var body = JSON.parse(data);
        const token = body["token"];
        var newQueueList = body["queuelist"];
        if (newQueueList.length != 0) {
          //Get Current Date
          let ts = Date.now();
          let date_ob = new Date(ts);
          let date = date_ob.getDate();
          let month = date_ob.getMonth() + 1;
          let year = date_ob.getFullYear();

          let monthStr = "";
          if (month <= 9) {
            monthStr = "0" + month.toString();
          } else {
            monthStr = month.toString();
          }

          let dateStr = "";
          if (date <= 9) {
            dateStr = "0" + date.toString();
          } else {
            dateStr = date.toString();
          }

          const fileName =
            "queuelist" + "-" + year + "-" + monthStr + "-" + dateStr + ".json";

          if (fs.existsSync("logs/" + "queuelist/" + fileName)) {
            fs.readFile(
              "logs/" + "queuelist/" + fileName,
              "utf8",
              (err, data) => {
                if (err) throw err;
                var queueChanged = JSON.parse(data);
                const queueChangedList = newQueueList.filter(
                  ({ hospitalNumber: a, amountQueueForWaiting: x }) =>
                    !queueChanged.some(
                      ({ hospitalNumber: b, amountQueueForWaiting: y }) =>
                        a === b && x === y
                    )
                );

                //=====เก็บ queue ที่เปลี่ยนลง logfile=====
                // var logData = {
                //   date: dateFormat(new Date(), "yyyy-mm-dd HH:MM:ss"),
                //   data: JSON.parse(queueChangedList)
                // };
                // logFile.createLogFile(
                //   "A",
                //   "queuechanged",
                //   "queuechanged",
                //   logData
                // );
                //=====================================

                if (queueChangedList.length != 0) {
                  console.log("Queue changed");
                  console.log(
                    "queue changed = " + queueChangedList.length + " queues"
                  );
                  console.log("==============================");

                  //=====ตรวจสอบจำนวนครั้งที่ส่ง Notification=====
                  const summaryFileName =
                    "summary" +
                    "-" +
                    year +
                    "-" +
                    monthStr +
                    "-" +
                    dateStr +
                    ".json";

                  fs.readFile(
                    "logs/" + "queuechanged/" + summaryFileName,
                    "utf8",
                    (err, data) => {
                      if (err) throw err;
                      var notiSize =
                        parseInt(JSON.parse(data)) + queueChangedList.length;
                      logFile.createLogFile(
                        "W",
                        "queuechanged",
                        "summary",
                        notiSize.toString()
                      );
                    }
                  );
                  //=====================================

                  // *****Send QueueNotification*****
                  sendQueueNotification(token, queueChangedList);
                  //=====Update queuelist ทั้งหมด=====
                  logFile.createLogFile(
                    "W",
                    "queuelist",
                    "queuelist",
                    newQueueList
                  );
                  //=================================
                } else {
                  console.log("Queue not change");
                  console.log("==============================");
                }
              }
            );
          } else {
            //Create new log file
            //*****Send QueueNotification*****
            sendQueueNotification(token, newQueueList);
            logFile.createLogFile("W", "queuelist", "queuelist", newQueueList);

            logFile.createLogFile(
              "W",
              "queuechanged",
              "summary",
              newQueueList.length.toString()
            );
          }
        } else {
          console.log("***Out of queue***");
          console.log("==============================");
        }
      });
    });

    getReq.on("error", error => {
      console.error(error);
      API.LINE_NOTIFY.send({
        message: "⚠️ มีปัญหาบางอย่างเกิดขึ้น ไม่สามารถส่งแจ้งเตือนคิวได้"
      });
    });
    getReq.end();
  }
};

function sendQueueNotification(token, queueNoti) {
  var logdata = {
    date: dateFormat(new Date(), "yyyy-mm-dd HH:MM:ss"),
    data: []
  };

  // console.log(queueNoti.length);

  queueNoti.forEach(element => {
    var jsonBody = JSON.stringify(element);
    const sendNotification = {
      host: API.SCB_NOTIFICATION_HOST,
      path: API.SCB_NOTIFICATION_PATH + "receiveQueueNotification",
      method: "POST",
      headers: {
        Authorization: "Bearer " + token,
        "Content-Type": "application/json",
        lang: "TH",
        "Content-Length": Buffer.byteLength(jsonBody)
      }
    };

    logdata["data"].push(JSON.parse(jsonBody));
    const postReq = https.request(sendNotification);
    // const postReq = https.request(sendNotification, response =>{
    //   response.setEncoding("utf8");
    //   response.on("data", function(chunk) {
    //     console.log(chunk);
    //   });
    // });

    postReq.on("error", error => {
      console.error(error);
    });
    // //Sending...request body
    postReq.write(jsonBody);
    postReq.end();
  });

  logFile.createLogFile("A", "queuenotification", "queuenotification", logdata);
}
