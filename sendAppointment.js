const API = require("./config");
const https = require("https");
https.globalAgent.maxSockets = 5;

const fs = require("fs");
const logFile = require("./createLogFile");
//npm install
const dateFormat = require("dateformat");

//==================Since 2019-09-22======================//
// Created By: Famz
// Remark: หาต้องการสลับไปใช้ระบบ Test ให้เปลี่ยน https เป็น http ใน func. sendNotification และเปลี่ยน Host เป็นเลข IP: 203.154.116.83
//=======================================================//

//***Call service from Hospital***//
exports.send = () => {
  getAppointmentData();
  function getAppointmentData() {
    var urldata = {
      host: API.HOSPITAL_SERVICE_HOST,
      path: API.HOSPITAL_SERVICE_PATH + "sendAppointmentNotification",
      method: "POST",
      rejectUnauthorized: false
    };

    const getReq = https.request(urldata, response => {
      var data = ""; //This will store the page we're downloading.
      response.on("data", chunk => {
        //Executed whenever a chunk is received.
        data += chunk; //Append each chunk to the data variable.
      });

      response.on("end", () => {
        var body = JSON.parse(data);
        const token = body["token"];

        if (body["appointmentlist"] != null) {
          if (body["appointmentlist"].length != 0) {
            body["appointmentlist"].forEach(appointment => {
              var jsonBody = JSON.stringify(appointment);
              var logData = {
                date: dateFormat(new Date(), "yyyy-mm-dd HH:MM:ss"),
                data: JSON.parse(jsonBody)
              };
              sendAppointmentNotification(token, jsonBody);
              logFile.createLogFile("A", "appointment", "appointment", logData);
            });

            //====== Send to LineNotify ======
            // console.log("Status: " + global.STATUS);
            // API.LINE_NOTIFY.send({
            //   message: "ส่งแจ้งเตือนนัดหมายสำเร็จ"
            // }).then(console.log);
            //=================================
          } else {
            console.log("***Out of appointment***");
            console.log("==============================");

            // API.LINE_NOTIFY.send({
            //   message: "ไม่มีนัดหมาย"
            // }).then(console.log);
          }
        }

        setTimeout(sendLineNotify, 20000);
        // sendLineNotify();
      });
    });

    // getReq.on("end", () =>{
    //   sendLineNotify();
    // });

    getReq.on("error", error => {
      console.error(error);
      API.LINE_NOTIFY.send({
        message: "⚠️ มีปัญหาบางอย่างเกิดขึ้น ไม่สามารถส่งแจ้งเตือนนัดหมายได้"
      });
      // logFile.createLogFile("A", "error", error);
    });

    getReq.end();
  }
};

function sendAppointmentNotification(token, jsonBody) {
  //***Send notification to SCB notification service***//
  const sendNotification = {
    host: API.SCB_NOTIFICATION_HOST,
    path: API.SCB_NOTIFICATION_PATH + "receiveAppointmentNotification",
    method: "POST",
    headers: {
      Authorization: "Bearer " + token,
      "Content-Type": "application/json",
      lang: "TH",
      "Content-Length": Buffer.byteLength(jsonBody)
    }
  };

  var postReq = https.request(sendNotification, res => {
    var notiStatusCode;
    var data;
    res.on("data", chunk => {
      notiStatusCode = res.statusCode;
      data += chunk;
    });

    res.on("end", () => {
      // console.log(notiStatusCode);
      // console.log(data);
      if (notiStatusCode == "200") {
        var successBody = {
          statuscode: res.statusCode,
          date: dateFormat(new Date(), "yyyy-mm-dd HH:MM:ss"),
          HN: JSON.parse(jsonBody)["hospitalNumber"],
          result: Buffer.from(data).toString()
        };
        logFile.createLogFile(
          "A",
          "success",
          "appointment_success",
          successBody
        );
      } else {
        var errorBody = {
          statuscode: res.statusCode,
          date: dateFormat(new Date(), "yyyy-mm-dd HH:MM:ss"),
          HN: JSON.parse(jsonBody)["hospitalNumber"],
          result: Buffer.from(data).toString()
        };
        logFile.createLogFile("A", "error", "appointment_error", errorBody);
      }
    });
  });

  postReq.on("error", error => {
    console.error(error);
  });

  postReq.write(jsonBody);
  postReq.end();
}

function sendLineNotify() {
  let ts = Date.now();
  let date_ob = new Date(ts);
  let date = date_ob.getDate();
  let month = date_ob.getMonth() + 1;
  let year = date_ob.getFullYear();

  let monthStr = "";
  if (month <= 9) {
    monthStr = "0" + month.toString();
  } else {
    monthStr = month.toString();
  }

  let dateStr = "";
  if (date <= 9) {
    dateStr = "0" + date.toString();
  } else {
    dateStr = date.toString();
  }

  const successFileName =
    "appointment_success" +
    "-" +
    year +
    "-" +
    monthStr +
    "-" +
    dateStr +
    ".json";
  const errorFileName =
    "appointment_error" + "-" + year + "-" + monthStr + "-" + dateStr + ".json";

  const errorFileExist = fs.existsSync("logs/error/" + errorFileName);
  const successFileExist = fs.existsSync("logs/success/" + successFileName);
  try {
    if (!errorFileExist && successFileExist) {
      //file exists
      API.LINE_NOTIFY.send({
        message: "📅 ส่งแจ้งเตือนนัดหมายสำเร็จ"
      });
    } else if (errorFileExist && successFileExist) {
      API.LINE_NOTIFY.send({
        message:
          "📅 ส่งแจ้งเตือนนัดหมายสำเร็จ...แต่มีผู้ป่วยบางรายที่ไม่ได้รับการแจ้งเตือนนี้ กรุณาตรวจสอบหมายเลขผู้ป่วยที่ไม่ได้รับการแจ้งเตือนและทำการส่งแจ้งเตือนใหม่อีกครั้ง!"
      });
    } else if (!successFileExist) {
      API.LINE_NOTIFY.send({
        message:
          "💥 ส่งแจ้งเตือนนัดหมายไม่สำเร็จ...กรุณาตรวจสอบการเรียกใช้งาน notification service และ การทำงานของ server"
      });
    }
  } catch (err) {
    console.error(err);
    API.LINE_NOTIFY.send({
      message:
        "💥 ส่งแจ้งเตือนนัดหมายไม่สำเร็จ...กรุณาตรวจสอบการเรียกใช้งาน notification service และ การทำงานของ server"
    });
  }
}
