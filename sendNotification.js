//Call notification function
const dateFormat = require("dateformat");
const sendAppointment = require("./sendAppointment");
const sendMasterDataClinicCode = require("./sendMasterDataClinicCode");
const sendQueue = require("./sendQueue");

//Cron Job
const appointment_clinicCode = require("node-cron");
const queue = require("node-cron");

function SendNotification() {
  // sendAppointment.send();
  appointment_clinicCode.schedule("0 8 * * *", () => {
    var time = dateFormat(new Date(), "yyyy-mm-dd HH:MM:ss");
    console.log(time + " => Send Appointment Notification");
    sendAppointment.send();
    
    console.log(time + " => Send MasterDataClinicCode");
    sendMasterDataClinicCode.send();
  });

  queue.schedule("* 8-19 * * *", () => {
    var time = dateFormat(new Date(), "yyyy-mm-dd HH:MM:ss");
    console.log(time + " => Send Queue Notification");
    sendQueue.send();
  });
}

SendNotification();