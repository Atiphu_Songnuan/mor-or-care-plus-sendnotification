const fs = require("fs");
exports.createLogFile = (mode, foldername, fileName, data) => {
  //Get Current Date
  let ts = Date.now();
  let date_ob = new Date(ts);
  let date = date_ob.getDate();
  let month = date_ob.getMonth() + 1;
  let year = date_ob.getFullYear();

  let monthStr = "";
  if (month <= 9) {
    monthStr = "0" + month.toString();
  } else {
    monthStr = month.toString();
  }

  let dateStr = "";
  if (date <= 9) {
    dateStr = "0" + date.toString();
  } else {
    dateStr = date.toString();
  }

  const logFile = fileName + "-" + year + "-" + monthStr + "-" + dateStr + ".json";

  if (mode == "A") {
    try {
      fs.appendFileSync(
        "logs/" + foldername + "/" + logFile,
        JSON.stringify(data) + "\n",
        function(err) {
          if (err) throw err;
          //file written successfully
        }
      );
    } catch (err) {
      console.error(err);
    }
  } else if (mode == "W") {
    try {
      fs.writeFileSync(
        "logs/" + foldername + "/" + logFile,
        JSON.stringify(data) + "\n",
        function(err) {
          if (err) throw err;
          //file written successfully
        }
      );
    } catch (err) {
      console.error(err);
    }
  }
};
